import React from 'react';
import $ from 'jquery';
import Apartment from "./Apartment";
import styles from "./css/body.css"
import {Accordion} from "react-bootstrap";
import RangeSlider from "./RangeSlider";
import ReactDOM from "react-dom";
import Multiselect from 'multiselect-react-dropdown';


export default class Body extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            apartments: [],
            maxFloor: 1,
            itemsCount: 0,
            minPrice:this.props.minPrice ?? 1000000000,
            maxPrice:this.props.maxPrice ?? 0,
            minPriceFilter: 0,
            maxPriceFilter: 1000000000,
            apartmentList: [],
            orderBy: 'id ASC',
            floorArray: [1,2,3,4],
            roomsArray: [1, 2, 3],
            notLastFloor: false,
            roomsPickArray: [true, true, true],
            itemsOnPage: 8,
            pagination: [],
            currentPage: 1,
            setDefault: false,
        };
        this.updatePrice = this.updateMinMaxPrice.bind(this);
        this.updateOrderBy = this.updateOrderBy.bind(this);
        this.lastFloorChange = this.lastFloorChange.bind(this);
        this.updateRoomsNumber = this.updateRoomsNumber.bind(this);
    }

    componentDidMount() {
        this.fetch();
    }

    lastFloorChange() {
        let floorArray = this.state.floorArray;
        this.state.notLastFloor ? floorArray.push(this.state.maxFloor):floorArray.pop()  ;
        this.setState({
            notLastFloor: !this.state.notLastFloor,
            floorArray: floorArray
        })
        this.fetch();
    }

    updateRoomsNumber(index){
        let roomsArray = this.state.roomsArray;
        let roomsPickArray = this.state.roomsPickArray;
        let setDefault = false;

        if (roomsArray.includes(index+1) && roomsArray.length!==1){
            roomsArray.splice(roomsArray.indexOf(index+1),1)
            roomsPickArray[index] = !roomsPickArray[index];
            console.log('1rst case');
            this.setState({
                roomsPickArray: roomsPickArray ,
                roomsArray: roomsArray
            })
        }
        else if(roomsArray.includes(index+1) && roomsArray.length===1){
            roomsArray = [1,2,3];
            roomsPickArray = [true, true,true];
            this.setState({
                roomsPickArray: roomsPickArray ,
                roomsArray: roomsArray,
            })
            setDefault = true;
        }
        else {
            roomsArray.push(index+1)
            roomsPickArray[index] = !roomsPickArray[index];
            console.log('3rd case');
            this.setState({
                roomsPickArray: roomsPickArray ,
                roomsArray: roomsArray
            });
         }

        this.fetch(setDefault);

    }

    updateMinMaxPrice(min, max){
        this.setState(
            {
                minPriceFilter: min,
                maxPriceFilter: max,
                currentPage: 1
            }
        )

        $("#RangeSliderMin").text(`${this.state.minPriceFilter}руб.`);
        $("#RangeSliderMax").text(`${this.state.maxPriceFilter}руб.`);
        this.fetch();
    }

    changePage(i) {
        this.setState({
            currentPage: i
        })
        this.fetch();
    }

    updateOrderBy(orderBy) {
        let sort = "";
        if(this.state.orderBy.includes(orderBy)){
            sort = orderBy;
            sort += this.state.orderBy.includes('ASC') ? ' DESC' : ' ASC';
        }
        else sort = orderBy + ' ASC';
        this.setState(
            {
                orderBy: sort,
            }
        )
        this.fetch();

    }

    fetch(setDefault = false) {
        const context = this;

        $.ajax({
            url: !setDefault? `http://localhost:8080/api/filtered` : `http://localhost:8080/api` ,
            data:{
                minPrice: this.state.minPriceFilter,
                maxPrice: this.state.maxPriceFilter,
                orderBy: this.state.orderBy,
                floorArray: this.state.floorArray,
                roomsArray: this.state.roomsArray,
                page: this.state.currentPage,
                pageSize: this.state.itemsOnPage,
            },
            method: 'GET',
            success: function(response) {
                let children = [];
                for (const responseKey in response) {
                    children.push(response[responseKey]);
                }
                context.setState({
                    apartments: children,
                });
            }
        }).then(()=>{
            if(this.state.apartments.length){
                const startIndex = (this.state.currentPage-1)*this.state.itemsOnPage;
                const lastIndex =  startIndex+parseInt(this.state.itemsOnPage);

                let apartments = this.state.apartments.slice(startIndex, lastIndex);

                let localMin = this.state.minPrice;
                let localMax = this.state.maxPrice;
                let localMaxFloor = this.state.maxFloor;
                let apartmentsCount = this.state.apartments.length;

                apartments.forEach((apartment)=>{
                    localMaxFloor = apartment.floor > localMaxFloor ? apartment.floor : localMaxFloor;
                    localMin = apartment.price < localMin ? apartment.price : localMin;
                    localMax = apartment.price > localMax ? apartment.price : localMax;
                })
                this.setState(
                    {
                        maxFloor: localMaxFloor,
                        minPrice: localMin,
                        maxPrice: localMax,
                        itemsCount: apartmentsCount
                    }
                )

                let currentApartmentList = []

                for (const apartmentId in apartments) {
                    currentApartmentList.push(
                        <Apartment
                            id={apartments[apartmentId].id}
                            floor={apartments[apartmentId].floor}
                            max_floor={this.state.maxFloor}
                            itemsCount = {this.state.itemsCount}
                            pos_on_floor = {apartments[apartmentId].pos_on_floor}
                            price = {apartments[apartmentId].price}
                            rooms = {apartments[apartmentId].rooms}
                            area_total = {apartments[apartmentId].area_total}
                            area_live = {apartments[apartmentId].area_live}
                            area_kitchen = {apartments[apartmentId].area_kitchen}
                            layout_image = {apartments[apartmentId].layout_image}
                        />,
                    )
                }
                let currentpagination = [];
                const pageCount = Math.ceil( this.state.itemsCount / this.state.itemsOnPage)
                for (let i = 1; i <= pageCount; i++){
                    currentpagination.push(
                        <button className="btn btn-default" onClick={() =>  this.changePage(i)}>
                            <span className={this.state.currentPage===i ? 'current-page':''}>
                                {i}
                            </span>
                        </button>
                    );
                }
                if(!$("#RangeSliderMin").text()) {
                    $("#RangeSliderMin").text(`${this.state.minPrice}руб.`);
                    $("#RangeSliderMax").text(`${this.state.maxPrice}руб.`);
                }

                this.setState({
                    apartmentList: currentApartmentList,
                    pagination: currentpagination
                })
                if(setDefault){
                    this.setState({
                        orderBy: 'id ASC',
                        floorArray: [1,2,3,4],
                        roomsArray: [1, 2, 3],
                        notLastFloor: false,
                        roomsPickArray: [true, true, true],
                        itemsOnPage: 8,
                        currentPage: 1,
                        maxPriceFilter: this.state.maxPrice,
                        minPriceFilter: this.state.minPrice
                    })
                    $("#RangeSliderMin").text(`${this.state.minPrice}руб.`);
                    $("#RangeSliderMax").text(`${this.state.maxPrice}руб.`);

                    let leftDot = $('.css-eg0mwd-MuiSlider-thumb')[0];
                    let rightDot = $('.css-eg0mwd-MuiSlider-thumb')[1];
                    let slide = $('.css-1gv0vcd-MuiSlider-track')[0];

                    $(leftDot).css({left: "0%"});
                    $(rightDot).css({left: "100%"});
                    $(leftDot).css({left: "0%"});
                    $(slide).css({left: "0%", width: "100%"});
                }
            }
        })

    }

    render() {
            return (
                <div>
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <h2>Выбор квартир</h2>
                                <div className="row">
                                    <div className="col">
                                        <Accordion  >
                                            <Accordion.Item eventKey="1">
                                                <div className="row">
                                                    <div className="col-8">
                                                        <div className="row">
                                                            <div className="col">
                                                                <div className="row">
                                                                    <div className="col">
                                                                        Сортировка
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col">
                                                                        <div className="toolbar">
                                                                            <button className="btn btn-default" onClick={() => this.updateOrderBy('price')}>
                                                                                {this.state.orderBy === 'price ASC' ? "↑" : ''}
                                                                                {this.state.orderBy === 'price DESC' ? "↓" : ''}
                                                                                <span id="sort-price"/>  Цена
                                                                            </button>
                                                                            <button className="btn btn-default" onClick={() => this.updateOrderBy('floor')}>
                                                                                {this.state.orderBy === 'floor ASC' ? "↑" : ''}
                                                                                {this.state.orderBy === 'floor DESC' ? "↓" : ''}
                                                                                <span id="sort-floor"/>  Этаж
                                                                            </button>
                                                                            <button className="btn btn-default" onClick={() => {
                                                                                this.fetch(true);
                                                                            }}>
                                                                                <span id="sort-area"/>  x
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col">
                                                                <div className="row">
                                                                    <div className="col">
                                                                        Количество комнат
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col">
                                                                        <div className="row">
                                                                            <div className="col-auto">
                                                                                <div onClick={()=>this.updateRoomsNumber(0)}>
                                                                                    <span className={this.state.roomsPickArray[0] ? "chechBox chechBox-selected" : "chechBox chechBox-notSelected"}>1</span>
                                                                                </div>
                                                                            </div>
                                                                            <div className="col-auto">
                                                                                <div onClick={()=>this.updateRoomsNumber(1)}>
                                                                                    <span className={this.state.roomsPickArray[1] ? "chechBox chechBox-selected" : "chechBox chechBox-notSelected"}>2</span>
                                                                                </div>
                                                                            </div>
                                                                            <div className="col-auto">
                                                                                <div onClick={()=>this.updateRoomsNumber(2)}>
                                                                                    <span className={this.state.roomsPickArray[2] ? "chechBox chechBox-selected" : "chechBox chechBox-notSelected"}>3</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col">
                                                                <div className="row">
                                                                    <div className="col">
                                                                        Стоимость
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col">
                                                                        <span id={'RangeSliderMin'}/>
                                                                    </div>
                                                                    <div className="col">
                                                                        <span id={'RangeSliderMax'}/>
                                                                    </div>

                                                                </div>
                                                                <div className="row">
                                                                    <div className="col">
                                                                        {
                                                                            (this.state.minPrice < this.state.maxPrice) ?
                                                                            <RangeSlider
                                                                                min={this.state.minPrice}
                                                                                max={this.state.maxPrice}
                                                                                update={this.updatePrice}
                                                                                setDefault={false}
                                                                            />:<></>
                                                                        }
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-4">
                                                        <span>Всего планировок: {this.state.itemsCount} </span>
                                                        <Accordion.Header>Расширенный фильтр</Accordion.Header>
                                                    </div>
                                                </div>
                                                <Accordion.Body>
                                                    <div className="row">
                                                        <div className="col-auto">
                                                            <div className={this.state.notLastFloor ? 'additional-filters additional-filters-active' : 'additional-filters'} onClick={this.lastFloorChange}>
                                                                <span>Не последний этаж</span>
                                                                <input type="checkbox" name="not-last-floor" hidden={true} checked={this.state.notLastFloor}  onChange={this.lastFloorChange} id="not-last-floor"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Accordion.Body>
                                            </Accordion.Item>
                                        </Accordion>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="body">
                        <div className="container">
                            <div className="row">
                                <div className="col">
                                    {this.state.apartments.length  ? this.state.pagination : ''}
                                </div>
                            </div>
                            <div className="row justify-content-center">
                                {
                                    this.state.apartments.length ?
                                        this.state.apartmentList :
                                        <div className={'col'}>
                                            Ничего не нашлось. Попробуйте изменить условия поиска или <button className={"btn btn-default"} onClick={()=>this.fetch(true)}>сбросить их</button>
                                        </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>

            )
        }
        /*else return (
            <div>
                123
            </div>
        )*/




}