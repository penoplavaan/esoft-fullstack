import React, {useState} from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import styles from './css/apartment.css'
import $ from 'jquery';
import ModalExample from "./Popup";

function numberWithSpaces(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

export default class Apartment extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="col-sm"  key={this.props.id} >
                <Card className={'card'}>
                    <Card.Body>
                        <Card.Title>{this.props.rooms}-к квартира</Card.Title>
                        <ModalExample
                            id = {this.props.id}
                            imagesrc ={this.props.layout_image}
                            building_number={202}
                            floor = {this.props.floor}
                            rooms = {this.props.rooms}
                            max_floor = {this.props.max_floor}
                            area_total = {this.props.area_total}
                            area_live = {this.props.area_live}
                            area_kitchen = {this.props.area_kitchen}
                            price = {this.props.price}
                            pos_on_floor = {this.props.pos_on_floor}
                        />

                        <Card.Subtitle>{this.props.area_total}м2, на {this.props.floor} этаже</Card.Subtitle>
                        <Card.Text>
                            От {numberWithSpaces(this.props.price)} руб.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </div>
        )
    }

}