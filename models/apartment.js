const {request} = require("express");
const Pool = require('pg').Pool;

const pool = new Pool({
    user: 'penoplavaan',
    host: '127.0.0.1',
    database: 'esoft-house',
    password: 'elephant3rEe',
    port: 5432,
});

const getApartments = () => {
    return new Promise(function(resolve, reject) {
        let queryString = `SELECT * FROM apartments ORDER BY id`;

        pool.query(queryString, (error, results) => {
            if (error) {
                //reject(error)
                resolve([]);
            }
            resolve(results.rows);
            //console.log(results.rows)
        })
    })
}

const getApartmentsBetween = (min, max, orderBy, floorArray, roomsArray) => {
    //console.log('initial values: ');
    //console.log(min, max, orderBy, floorArray, roomsArray);

    let floorTextArr = "[";
    for (const floorArrKey in floorArray) {
        floorTextArr +=parseInt(floorArray[floorArrKey]) +",";
    }
    floorTextArr = floorTextArr.slice(0, -1);
    floorTextArr += "]";

    let roomsTextArray = "[";
    for (const roomsArrayKey in roomsArray) {
        roomsTextArray += parseInt(roomsArray[roomsArrayKey]) + ","
        //console.log(roomsTextArray);
    }
    //console.log(roomsTextArray);
    roomsTextArray = roomsTextArray.slice(0, -1);
    roomsTextArray += "]";


    //console.log(roomsTextArray);
    return new Promise(function(resolve, reject) {
        let queryString = `SELECT * FROM apartments WHERE floor = ANY(ARRAY${floorTextArr}) AND rooms = ANY(ARRAY${roomsTextArray}) AND price BETWEEN ${min} AND ${max} ORDER BY ${orderBy}`;
        if (roomsArray.length === 0) queryString = `SELECT * FROM apartments WHERE floor = ANY(ARRAY${floorTextArr}) AND price BETWEEN ${min} AND ${max} ORDER BY ${orderBy}`;
        //console.log(queryString);
        pool.query(queryString, (error, results) => {
            if (error) {
                //reject(error)
                resolve([]);
            }
            resolve(results.rows);
            //console.log(results.rows)
        })
    })
}

const createApartment = (body) => {
    return new Promise(function(resolve, reject) {
        const { floor, pos_on_floor, price, rooms, area_total, area_kitchen, area_live, layout_image } = body;
        pool.query(
            'INSERT INTO apartments (floor, pos_on_floor, price, rooms, area_total, area_kitchen, area_live, layout_image) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *',
            [floor, pos_on_floor, price, rooms, area_total, area_kitchen, area_live, layout_image],
            (error, results) => {
                if (error) {
                    reject(error)
                }
                resolve(`A new apartment has been added added: ${results.rows[0]}`)
            }
        )
    })
}
const deleteMerchant = () => {
    return new Promise(function(resolve, reject) {
        const id = parseInt(request.params.id)
        pool.query('DELETE FROM merchants WHERE id = $1', [id], (error, results) => {
            if (error) {
                reject(error)
            }
            resolve(`Merchant deleted with ID: ${id}`)
        })
    })
}

module.exports = {
    getApartments,
    getApartmentsBetween,
    createApartment,
    deleteMerchant,
}