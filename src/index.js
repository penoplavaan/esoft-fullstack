import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Body from "./Companents/Body";
import Header from "./Companents/Header";
import Controls from "./Companents/Controls";


ReactDOM.render(
  <React.StrictMode>
      <div>
          <Header />
          <Body />
      </div>
  </React.StrictMode>,
  document.getElementById('root')
);




