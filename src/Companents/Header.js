import React from 'react';
import styles from './css/header.css'

export default class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'container'}>
                <nav className="navbar sticky-top navbar-light">
                    <a className="navbar-brand" href="#">
                        MALINKA
                    </a>
                </nav>
            </div>
        )
    }
}