const express = require('express');
const app = express();
const http = require('http');
const url = require('url');

const apartment_model = require('./models/apartment');


app.get('/api', (req, res) => {
    res.set('Access-Control-Allow-Origin', '*');
    apartment_model.getApartments()
        .then(response => {
            res.status(200).send(response);
        })
        .catch(error => {
            res.status(500).send(error);
        })
})

app.get('/api/filtered', (req, res) => {
    //console.log(req.query);
    res.set('Access-Control-Allow-Origin', '*');
    console.log(req.query);
    apartment_model.getApartmentsBetween(req.query.minPrice,req.query.maxPrice,req.query.orderBy, req.query.floorArray, req.query.roomsArray)
        .then(response => {
            res.status(200).send(response);
        })
        .catch(error => {
            res.status(500).send(error);
        })
})


app.listen(8080, () => {
    console.log('listening on port 8080')
})