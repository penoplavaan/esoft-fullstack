import * as React from 'react';
import Slider from '@mui/material/Slider';


export default function RangeSlider(props) {
    console.log(props.setDefault);

    const [value, setValue] = React.useState([props.min, props.max]);

    function numberWithSpaces(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }

    const handleChange = (event, newValue) => {
        setValue(newValue);
        props.update(newValue[0], newValue[1]);
    };

    return (
        <div>

            <Slider
                min={props.min}
                max={props.max}
                value={!props.setDefault? value : [props.min, props.max]}
                valueLabelDisplay="auto"
                onChange={handleChange}
            />
        </div>
    );
}
